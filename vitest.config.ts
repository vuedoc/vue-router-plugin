// eslint-disable-next-line import/extensions
import { defineConfig } from 'vitest/config';

export default defineConfig({
  test: {
    deps: {
      inline: [
        '@vuedoc/test-utils',
      ],
    },
    setupFiles: [
      '@vuedoc/test-utils',
      'test/setup.js',
    ],
  },
});
