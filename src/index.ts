import { PluginInterface } from '@vuedoc/parser';

export const VueRouterPlugin: PluginInterface = () => {
  return {
    resolver: {
      alias: {
        'vue-router': 'vue-router/dist/vue-router.d.ts',
      },
    },
    composition: {
      data: [
        {
          fname: 'useRoute',
          returningValue: '',
        },
        {
          fname: 'useRouter',
          returningValue: '',
        },
      ],
      computed: [
        {
          fname: 'useLink',
        },
      ],
    },
  };
};
