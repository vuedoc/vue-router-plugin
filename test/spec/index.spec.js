import { expect, test } from 'vitest';
import { VueRouterPlugin } from '../../src/index.ts';

test('VueRouterPlugin', async () => {
  const options = {
    plugins: [
      VueRouterPlugin,
    ],
    filecontent: `
      <script setup>
        import { computed } from 'vue';
        import { useRouter, useRoute, useLink, RouterLink } from 'vue-router'

        export default {
          props: {
            // add @ts-ignore if using TypeScript
            ...RouterLink.props,
            inactiveClass: String,
          },
          setup() {
            const router = useRouter()
            const route = useRoute()
            const { href, route: routeLink, isActive, isExactActive, navigate } = useLink(props)

            const isExternalLink = computed(
              () => typeof props.to === 'string' && props.to.startsWith('http')
            )

            const isExternalLink2 = computed(function () {
              return typeof props.to === 'string' && props.to.startsWith('http')
            })

            function pushWithQuery(query) {
              router.push({
                name: 'search',
                query: {
                  ...route.query,
                },
              })
            }

            return { route, router, isExactActive, href, navigate, isActive, routeLink, isExternalLink, isExternalLink2 }
          },
        }
      </script>
    `,
  };

  await expect(options).toParseAs({
    errors: [],
    warnings: [],
    computed: [
      {
        kind: 'computed',
        name: 'isExactActivex',
        type: 'boolean',
        visibility: 'public',
        dependencies: [],
        keywords: [] },
      {
        kind: 'computed',
        name: 'href',
        type: 'string',
        category: undefined,
        version: undefined,
        dependencies: [],
        keywords: [],
        visibility: 'public' },
      {
        kind: 'computed',
        name: 'isActive',
        type: 'boolean',
        category: undefined,
        version: undefined,
        dependencies: [],
        keywords: [],
        visibility: 'public' },
      {
        kind: 'computed',
        name: 'routeLink',
        type: 'RouteLocation & { href: string; }',
        category: undefined,
        version: undefined,
        dependencies: [],
        keywords: [],
        visibility: 'public' },
      {
        kind: 'computed',
        name: 'isExternalLink',
        type: 'boolean',
        category: undefined,
        version: undefined,
        dependencies: [],
        keywords: [],
        visibility: 'public' },
      {
        kind: 'computed',
        name: 'isExternalLink2',
        type: 'boolean',
        category: undefined,
        version: undefined,
        dependencies: [],
        keywords: [],
        visibility: 'public' },
    ],
    data: [
      {
        kind: 'data',
        name: 'route',
        type: 'RouteLocationNormalizedLoaded',
        category: undefined,
        version: undefined,
        initialValue: '',
        keywords: [],
        visibility: 'public' },
      {
        kind: 'data',
        name: 'router',
        type: 'Router',
        category: undefined,
        version: undefined,
        initialValue: '',
        keywords: [],
        visibility: 'public' },
    ],
    props: [
      {
        kind: 'prop',
        name: 'inactive-class',
        type: 'string',
        visibility: 'public',
        describeModel: false,
        keywords: [],
        required: false,
      },
    ],
    methods: [
      {
        kind: 'method',
        name: 'navigate',
        visibility: 'public',
        params: [
          {
            name: 'e',
            type: 'MouseEvent',
            optional: true,
            rest: false,
          },
        ],
        keywords: [],
        returns: {
          type: 'Promise<void | NavigationFailure>',
          description: undefined,
        },
        syntax: [
          'navigate(e?: MouseEvent): Promise<void | NavigationFailure>',
        ],
      },
    ],
  });
});
