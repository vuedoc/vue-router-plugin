## Vue Router Vuedoc Plugin 1.0.0-beta2

This release fixes packaging by removing useless files.

## Vue Router Vuedoc Plugin 1.0.0-beta1

This is the first release of the Vue Router plugin for Vuedoc.
